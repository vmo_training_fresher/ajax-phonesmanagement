package com.vmo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhonesManagementAjaxWebServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhonesManagementAjaxWebServiceApplication.class, args);
    }

}

